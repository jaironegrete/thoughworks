package com.thoughworks.exercise.train.model;

public class City {
	
	private String name;
	
	public City(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
