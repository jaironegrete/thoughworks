package com.thoughworks.exercise.train.main;

import java.util.List;
import java.util.Scanner;

import com.thoughworks.exercise.train.exception.EmptyGraphException;
import com.thoughworks.exercise.train.exception.GraphNotFound;
import com.thoughworks.exercise.train.exception.InputException;
import com.thoughworks.exercise.train.load.LoadFile;
import com.thoughworks.exercise.train.model.TrainRoute;
import com.thoughworks.exercise.train.service.RouteService;

public class Menu {
	
	private static final String LOAD_MESSAGE = "Insert the file path: (default:graph.txt)";
	private static final String MENU_HEADER = "\nMEMU: write the number of the option";
	private static final String DISTANCE_ROUTES = "1: The distance between routes.";
	private static final String NUMBER_TRIPS = "2: The number of different routes with conditional";
	private static final String LENGTH_ROUTES = "3: The length of the shortest route between cities";
	private static final String ADD_CITIES = "\nwrite the cities to calculate the route (example A-B-C)";
	private static final String ADD_CITIES_TWO = "\nwrite the cities to calculate the route (example C-C)";
	private static final String ADD_CITIES_SHORTEST = "\nwrite the cities to calculate the shortest route (example C-C)";
	private static final String ADD_OPERATOR = "\nwrite an operator to calculate the route (only available \"LESS_THAN\", \"EQUALS\")";
	private static final String ADD_VALUE = "\nwrite the value of the condition to calculate the route";
	private static final String NO_ROUTE = "\nNO SUCH ROUTE";
	
	private Scanner sc = new Scanner(System.in);
	
	protected void execute() throws EmptyGraphException, GraphNotFound, InputException {
		String filePath = showMessages(LOAD_MESSAGE);
		String [] graph = LoadFile.readFilePath(filePath);
		List<TrainRoute> trainRouteList = LoadFile.castStringToTrainRoute(graph);
		String readOption = showMessages(MENU_HEADER, DISTANCE_ROUTES, NUMBER_TRIPS, LENGTH_ROUTES);
		String cities = "";
		String operator = "";
		int value = 0;
		int numberOfRoutes = 0;
		int distance = 0;
		switch (readOption) {
		case "1":
			cities = showMessages(ADD_CITIES);
			numberOfRoutes = RouteService.calculateDistance(cities, trainRouteList);
			System.out.println(numberOfRoutes >= 0 ? ("\nThe distance is: " + numberOfRoutes) : NO_ROUTE);
			break;
		case "2":
			cities = showMessages(ADD_CITIES_TWO);
			operator = showMessages(ADD_OPERATOR);
			value = Integer.valueOf(showMessages(ADD_VALUE));
			numberOfRoutes = RouteService.numberOfTrips(cities, trainRouteList, operator, value, "2");
			System.out.println(numberOfRoutes >= 0 ? ("\nThe number of different routes: " + numberOfRoutes) : NO_ROUTE);
			break;
		case "3":
			cities = showMessages(ADD_CITIES_SHORTEST);
			distance = RouteService.shortestDistanceBetweenCities(cities, trainRouteList, null, null, "3");
			System.out.println(distance >= 0 ? ("\nThe shortest distance between this cities are: " + distance) : NO_ROUTE);
			break;
		default:
			throw new InputException("Enter only available options");
		}
		closeSc();
		
	}
	
	private String showMessages(String... messages) {
		for (String message : messages) {
			System.out.println(message);
		}
		return sc.next().toUpperCase();
	}

	public Scanner getSc() {
		return sc;
	}

	public void closeSc() {
		this.sc.close();
	}
	
}
