package com.thoughworks.exercise.train.exception;

import java.io.IOException;

public class InputException extends IOException {

	private static final long serialVersionUID = -8650536363170608564L;

	public InputException(String message) {
		super(message);
	}
	
}
