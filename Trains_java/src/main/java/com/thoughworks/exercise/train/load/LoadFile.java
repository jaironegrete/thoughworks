package com.thoughworks.exercise.train.load;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.thoughworks.exercise.train.exception.EmptyGraphException;
import com.thoughworks.exercise.train.exception.GraphNotFound;
import com.thoughworks.exercise.train.model.City;
import com.thoughworks.exercise.train.model.TrainRoute;

public class LoadFile {
	
	private static final String DEFAULT_FILE = "graph.txt";
	private static final String DELIMITER = ",";
	private static final String DATA_ADDED = "\nData added:";
	
	private LoadFile() {
		throw new IllegalStateException("Load File class");
	}
	
	public static String[] readFilePath(String filePath) throws GraphNotFound {
		ClassLoader classLoader = LoadFile.class.getClassLoader();
		if(filePath.equalsIgnoreCase(DEFAULT_FILE)) {
			filePath = classLoader.getResource(filePath).getPath();
		}
		String[] graph = null;
		try (FileReader file = new FileReader(filePath);
			 BufferedReader br = new BufferedReader(file)){
			String graphLine = br.readLine();
			graph = graphLine.split(DELIMITER);
		}catch (IOException e) {
			throw new GraphNotFound();
		}
		return graph;
	}
	
	public static List<TrainRoute> castStringToTrainRoute(String[] graph) throws EmptyGraphException{
		List<TrainRoute> trainRouteList = new ArrayList<>();
		if(graph != null && graph.length > 0) {
			System.out.println(DATA_ADDED);
			for (int i = 0; i < graph.length; i++) {
				String route = graph[i].trim();
				TrainRoute trainRoute = new TrainRoute(new City(route.substring(0, 1)), new City(route.substring(1, 2)), Integer.valueOf(route.substring(2, route.length())));
				trainRouteList.add(trainRoute);
				System.out.println(trainRoute);
			}
		}else {
			throw new EmptyGraphException();
		}
		
		return trainRouteList;
	}
	
	
	
}
