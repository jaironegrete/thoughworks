package com.thoughworks.exercise.train.constant;

public enum OperatorEnum {

	LESS_THAN("LESS_THAN"),
	EQUALS("EQUALS"),
	GREATER_THAN("GREATER_THAN");

    private String operator;

    OperatorEnum(String operator) {
        this.operator = operator;
    }

    public String operator() {
        return operator;
    }
}
