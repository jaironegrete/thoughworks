package com.thoughworks.exercise.train.exception;

import java.io.IOException;

public class GraphNotFound extends IOException {

	private static final long serialVersionUID = 4504130756313823093L;
	
	private static final String NOT_FOUND_GRAPH = "Error: graph not found";
	
	public GraphNotFound() {
		super(NOT_FOUND_GRAPH);
	}
}
