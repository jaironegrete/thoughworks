package com.thoughworks.exercise.train.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.thoughworks.exercise.train.constant.OperatorEnum;
import com.thoughworks.exercise.train.constant.OptionEnum;
import com.thoughworks.exercise.train.model.TrainRoute;

public class RouteUtils {
	
	private RouteUtils() {
		throw new IllegalStateException("Route Utils class");
	}

	public static TrainRoute findByCityAAndCityB(String cityA, String cityB, List<TrainRoute> trainRouteList) {
		Optional<TrainRoute> route = trainRouteList.stream().
			    filter(p -> p.getOrigin().getName().equals(cityA) && p.getDestiny().getName().equals(cityB)).
			    findFirst();
		if(route.isPresent()) {
			return route.get();
		}else {
			return null;
		}
	}
	
	public static List<TrainRoute> findAllRoutesByCityA(String cityA, List<TrainRoute> trainRouteList) {
		return trainRouteList.stream().filter(p -> p.getOrigin().getName().equals(cityA)).collect(Collectors.toList());
	}
	
	public static List<List<TrainRoute>> findRoutesByOperatorAndLimit(List<List<TrainRoute>> routes, String operator, Integer limit, String option, String cityA, String cityB, List<TrainRoute> trainRouteList){
		if(routes.isEmpty()) {
			initArray(routes, cityA, trainRouteList);
		}
		for (List<TrainRoute> tempRoutes : routes) {
			List<List<TrainRoute>> newRoutes = new ArrayList<>();
			newRoutes.addAll(routes);
			if(returnValidation(operator, limit, option, cityB, tempRoutes)) {
				List<TrainRoute> routesFinded = RouteUtils.findAllRoutesByCityA(tempRoutes.get(tempRoutes.size() -1).getDestiny().getName(), trainRouteList);
				if(routesFinded != null && !routesFinded.isEmpty()) {
					newRoutes.remove(tempRoutes);	
					for (int i = 0; i < routesFinded.size(); i++) {
						List<TrainRoute> newList = new ArrayList<>();
						newList.addAll(tempRoutes);
						newList.add(routesFinded.get(i));
						newRoutes.add(newList);
					}
					return findRoutesByOperatorAndLimit(newRoutes, operator, limit, option, null, cityB, trainRouteList);
				}
			}else if(returnValidationRemove(operator, limit, option, cityB, tempRoutes)){
				newRoutes.remove(tempRoutes);
				return findRoutesByOperatorAndLimit(newRoutes, operator, limit, option, null, cityB, trainRouteList);
			}
		}
		return routes;
	}
	
	private static boolean returnValidation(String operator, Integer limit, String option, String cityB, List<TrainRoute> tempRoutes) {
		if(OptionEnum.TRIPS.option().equalsIgnoreCase(option)) {
			if(OperatorEnum.LESS_THAN.operator().equalsIgnoreCase(operator)) {
				return tempRoutes.size() < limit && !tempRoutes.get(tempRoutes.size() -1).getDestiny().getName().equals(cityB);
			}else if(OperatorEnum.EQUALS.operator().equalsIgnoreCase(operator)) {
				return tempRoutes.size() < limit || (tempRoutes.size() < limit && !tempRoutes.get(tempRoutes.size() -1).getDestiny().getName().equals(cityB)); 
			}else {
				return false;
			}
		}else if(OptionEnum.SHORTEST.option().equalsIgnoreCase(option)) {
			return !tempRoutes.get(tempRoutes.size() -1).getDestiny().getName().equals(cityB) && validateBucle(tempRoutes);
		}else if(OptionEnum.TRIPS_DISTANCE.option().equalsIgnoreCase(option)) {
			return validateDistance(tempRoutes, operator, limit) || (!tempRoutes.get(tempRoutes.size() -1).getDestiny().getName().equals(cityB) && validateDistance(tempRoutes, operator, limit));
		}
		return false;
	}
	
	private static boolean returnValidationRemove(String operator, Integer limit, String option, String cityB, List<TrainRoute> tempRoutes) {
		if(OptionEnum.TRIPS.option().equalsIgnoreCase(option)) {
			if(OperatorEnum.LESS_THAN.operator().equalsIgnoreCase(operator)) {
				return tempRoutes.size() >= limit && !tempRoutes.get(tempRoutes.size() -1).getDestiny().getName().equals(cityB);
			}else if(OperatorEnum.EQUALS.operator().equalsIgnoreCase(operator)) {
				return tempRoutes.size() >= limit && !tempRoutes.get(tempRoutes.size() -1).getDestiny().getName().equals(cityB); 
			}else {
				return false;
			}
		}else if(OptionEnum.TRIPS_DISTANCE.option().equalsIgnoreCase(option)) {
			return !tempRoutes.get(tempRoutes.size() -1).getDestiny().getName().equals(cityB) && validateDistance(tempRoutes, OperatorEnum.GREATER_THAN.operator(), limit);
		}
		return false;
	}
	
	private static boolean validateDistance(List<TrainRoute> tempRoutes, String operator, Integer limit) {
		int distance = 0;
		for (TrainRoute trainRoute : tempRoutes) {
			distance += trainRoute.getDistance();
		}
		return OperatorEnum.LESS_THAN.operator().equalsIgnoreCase(operator) ? (distance <= limit) : (distance > limit);
	}

	
	public static int shortestDistance(List<List<TrainRoute>> routes) {
		int shortestDistance = 0;
		int tempDistance = 0;
		for (List<TrainRoute> list : routes) {
			tempDistance = 0;
			for (TrainRoute trainRoute : list) {
				tempDistance += trainRoute.getDistance();
			}
			if(shortestDistance == 0) {
				shortestDistance = tempDistance;
			}
			if(tempDistance < shortestDistance) {
				shortestDistance = tempDistance;
			}
		}
		return shortestDistance;
	}
	
	private static boolean validateBucle(List<TrainRoute> tempRoutes) {
		if(tempRoutes.size() > 1) {
			if(tempRoutes.get(tempRoutes.size() -1).getDestiny().getName().equals(tempRoutes.get(tempRoutes.size() -2).getOrigin().getName()) && tempRoutes.get(tempRoutes.size() -1).getOrigin().getName().equals(tempRoutes.get(tempRoutes.size() -2).getDestiny().getName())){
				return false;
			}else {
				return true;
			}
		}else {
			return true;
		}
	}
	
	private static void initArray(List<List<TrainRoute>> routes, String cityA, List<TrainRoute> trainRouteList) {
		List<TrainRoute> routesFinded = RouteUtils.findAllRoutesByCityA(cityA, trainRouteList);
		for (TrainRoute trainRoute : routesFinded) {
			List<TrainRoute> a = new ArrayList<>();
			a.add(trainRoute);
			routes.add(a);
		}
	}

	public static List<List<TrainRoute>> findSubRoutes(List<List<TrainRoute>> routes, String cityB, int limit) {
		List<List<TrainRoute>> copyRoutes = new ArrayList<>();
		for (List<TrainRoute> list : routes) {
			int distance = 0;
			for (TrainRoute trainRoute : list) {
				distance += trainRoute.getDistance();
			}
			if(distance > limit) {
				copyRoutes.add(fixRouteList(list, cityB));
			}else {
				copyRoutes.add(list);
			}
		}
		
		return copyRoutes;
	}

	private static List<TrainRoute> fixRouteList(List<TrainRoute> list, String cityB) {
		List<TrainRoute> fixedList = new ArrayList<>();
		fixedList.addAll(list);
		for (int i = list.size() - 1; i >= 0; i--) {
			if(i == list.size() - 1 ) {
				fixedList.remove(i);
			}else if(!list.get(i).getDestiny().getName().equals(cityB)) {
				fixedList.remove(i);
			}else {
				break;
			}
		}
		return fixedList;
	}
}
