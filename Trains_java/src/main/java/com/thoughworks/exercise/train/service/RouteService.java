package com.thoughworks.exercise.train.service;

import java.util.ArrayList;
import java.util.List;

import com.thoughworks.exercise.train.exception.InputException;
import com.thoughworks.exercise.train.model.TrainRoute;
import com.thoughworks.exercise.train.util.RouteUtils;

public class RouteService {
	
	private final static String INPUT_EXCEPTION = "Enter the format provided in example";
	
	private RouteService() {
		throw new IllegalStateException("Route Service class");
	}

	public static int calculateDistance(String cities, List<TrainRoute> trainRouteList) throws InputException {
		String[] citiesArray = cities.split("-");
		if(citiesArray.length <= 1) {
			throw new InputException(INPUT_EXCEPTION);
		}
		int distance = 0;
		for (int i = 0; i < citiesArray.length -1; i++) {
			String cityA = citiesArray[i];
			String cityB = citiesArray[i+1];
			TrainRoute trainRouteFinded = RouteUtils.findByCityAAndCityB(cityA, cityB, trainRouteList);
			if(trainRouteFinded != null) {
				distance += trainRouteFinded.getDistance();
			}else {
				distance = -1;
				break;
			}
		}
		return distance;
	}
	
	public static int numberOfTrips(String cities, List<TrainRoute> trainRouteList, String operator, Integer limit, String option) throws InputException {
		String[] citiesArray = cities.split("-");
		if(citiesArray.length <= 1) {
			throw new InputException(INPUT_EXCEPTION);
		}
		String cityA = citiesArray[0];
		String cityB = citiesArray[1];
		List<List<TrainRoute>> routes = RouteUtils.findRoutesByOperatorAndLimit(new ArrayList<>(), operator, limit, option, cityA, cityB, trainRouteList);
		return routes.size();
	}
	
	public static int shortestDistanceBetweenCities(String cities, List<TrainRoute> trainRouteList, String operator, Integer limit, String option) throws InputException {
		String[] citiesArray = cities.split("-");
		if(citiesArray.length <= 1) {
			throw new InputException(INPUT_EXCEPTION);
		}
		String cityA = citiesArray[0];
		String cityB = citiesArray[1];
		List<List<TrainRoute>> routes = RouteUtils.findRoutesByOperatorAndLimit(new ArrayList<>(), operator, limit, option, cityA, cityB, trainRouteList);
		if(routes != null && !routes.isEmpty()) {
			return RouteUtils.shortestDistance(routes);
		}
		return -1;
	}
	
	public static int numberOfTripsWithDistance(String cities, List<TrainRoute> trainRouteList, String operator, Integer limit, String option) throws InputException {
		String[] citiesArray = cities.split("-");
		if(citiesArray.length <= 1) {
			throw new InputException(INPUT_EXCEPTION);
		}
		String cityA = citiesArray[0];
		String cityB = citiesArray[1];
		List<List<TrainRoute>> routes = RouteUtils.findRoutesByOperatorAndLimit(new ArrayList<>(), operator, limit, option, cityA, cityB, trainRouteList);
		routes = RouteUtils.findSubRoutes(routes, cityB, limit);
		return routes.size();
	}
	
}
