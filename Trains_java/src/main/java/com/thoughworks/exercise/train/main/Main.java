package com.thoughworks.exercise.train.main;

import com.thoughworks.exercise.train.exception.EmptyGraphException;
import com.thoughworks.exercise.train.exception.GraphNotFound;
import com.thoughworks.exercise.train.exception.InputException;

public class Main {
	
	public static void main(String[] args) throws EmptyGraphException, GraphNotFound, InputException {
		Menu menu = new Menu();
		menu.execute();
	}

}
