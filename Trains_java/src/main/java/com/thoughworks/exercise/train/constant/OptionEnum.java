package com.thoughworks.exercise.train.constant;

public enum OptionEnum {

	TRIPS("2"),
	SHORTEST("3"),
	TRIPS_DISTANCE("4");

    private String option;

    OptionEnum(String option) {
        this.option = option;
    }

    public String option() {
        return option;
    }
}
