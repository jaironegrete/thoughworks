package com.thoughworks.exercise.train.exception;

public class EmptyGraphException extends Exception {

	private static final long serialVersionUID = -2233763911459142998L;
	
	private static final String EMPTY_GRAPH = "Error: empty graph";
	
	public EmptyGraphException() {
		super(EMPTY_GRAPH);
	}

}
