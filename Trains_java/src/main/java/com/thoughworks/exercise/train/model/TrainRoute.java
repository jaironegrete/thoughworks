package com.thoughworks.exercise.train.model;

public class TrainRoute {
	
	private City origin;
	private City destiny;
	private int distance;
	
	public TrainRoute(City origin, City destiny, int distance) {
		super();
		this.origin = origin;
		this.destiny = destiny;
		this.distance = distance;
	}
	
	public TrainRoute(City origin, City destiny) {
		super();
		this.origin = origin;
		this.destiny = destiny;
	}

	public City getOrigin() {
		return origin;
	}
	public City getDestiny() {
		return destiny;
	}
	public int getDistance() {
		return distance;
	}

	@Override
	public String toString() {
		return "TrainRoute [origin=" + origin.getName() + ", destiny=" + destiny.getName() + ", distance=" + distance + "]";
	}
	
	
	
}
