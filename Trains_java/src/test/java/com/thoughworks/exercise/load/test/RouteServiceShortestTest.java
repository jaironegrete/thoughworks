package com.thoughworks.exercise.load.test;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;

import com.thoughworks.exercise.train.exception.EmptyGraphException;
import com.thoughworks.exercise.train.exception.GraphNotFound;
import com.thoughworks.exercise.train.load.LoadFile;
import com.thoughworks.exercise.train.model.TrainRoute;

public class RouteServiceShortestTest {

	@Test
	public void loadFileTest() throws GraphNotFound {
		String[] graph = LoadFile.readFilePath("graph.txt");
		assertNotNull(graph);
	}
	
	@Test(expected = GraphNotFound.class)
	public void faileLoadTest() throws GraphNotFound {
		String[] graph = LoadFile.readFilePath("asd");
		assertNotNull(graph);
	}
	
	@Test
	public void castStringToTrainRouteTest() throws GraphNotFound, EmptyGraphException {
		String [] graph = {"AB2", "BC5"};
		List<TrainRoute> trainRoute = LoadFile.castStringToTrainRoute(graph);
		assertNotNull(trainRoute);
	}
	
	@Test(expected = EmptyGraphException.class)
	public void emptyGraphTest() throws EmptyGraphException {
		String [] graph = null;
		List<TrainRoute> trainRoute = LoadFile.castStringToTrainRoute(graph);
		assertNotNull(trainRoute);
	}
}
