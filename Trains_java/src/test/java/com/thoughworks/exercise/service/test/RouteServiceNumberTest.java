package com.thoughworks.exercise.service.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.thoughworks.exercise.train.exception.InputException;
import com.thoughworks.exercise.train.model.City;
import com.thoughworks.exercise.train.model.TrainRoute;
import com.thoughworks.exercise.train.service.RouteService;

public class RouteServiceNumberTest {

	@Test
	public void numerOfTripsTestA() throws InputException {
		String cities = "C-C";
		List<TrainRoute> trainRouteList = returnRoutes();
		int trips = RouteService.numberOfTrips(cities, trainRouteList, "LESS_THAN", 3, "2");
		assertEquals(2, trips);
	}
	
	@Test
	public void numerOfTripsTestB() throws InputException {
		String cities = "A-C";
		List<TrainRoute> trainRouteList = returnRoutes();
		int trips = RouteService.numberOfTrips(cities, trainRouteList, "EQUALS", 4, "2");
		assertEquals(3, trips);
	}
	
	
	public List<TrainRoute> returnRoutes(){
		List<TrainRoute> trainRoutesList = new ArrayList<>();
		trainRoutesList.add(new TrainRoute(new City("A"), new City("B"), 5));
		trainRoutesList.add(new TrainRoute(new City("B"), new City("C"), 4));
		trainRoutesList.add(new TrainRoute(new City("C"), new City("D"), 8));
		trainRoutesList.add(new TrainRoute(new City("D"), new City("C"), 8));
		trainRoutesList.add(new TrainRoute(new City("D"), new City("E"), 6));
		trainRoutesList.add(new TrainRoute(new City("A"), new City("D"), 5));
		trainRoutesList.add(new TrainRoute(new City("C"), new City("E"), 2));
		trainRoutesList.add(new TrainRoute(new City("E"), new City("B"), 3));
		trainRoutesList.add(new TrainRoute(new City("A"), new City("E"), 7));
		
		return trainRoutesList;
	}
	
}
