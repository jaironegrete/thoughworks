package com.thoughworks.exercise.service.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.thoughworks.exercise.train.exception.InputException;
import com.thoughworks.exercise.train.model.City;
import com.thoughworks.exercise.train.model.TrainRoute;
import com.thoughworks.exercise.train.service.RouteService;

public class RouteServiceShortestTest {

	@Test
	public void shortestDistanceTestA() throws InputException {
		String cities = "A-C";
		List<TrainRoute> trainRouteList = returnRoutes();
		int shortestDistance = RouteService.shortestDistanceBetweenCities(cities, trainRouteList, null, null, "3");
		assertEquals(9, shortestDistance);
	}
	
	@Test
	public void shortestDistanceTestB() throws InputException {
		String cities = "B-B";
		List<TrainRoute> trainRouteList = returnRoutes();
		int shortestDistance = RouteService.shortestDistanceBetweenCities(cities, trainRouteList, null, null, "3");
		assertEquals(9, shortestDistance);
	}
	
	
	public List<TrainRoute> returnRoutes(){
		List<TrainRoute> trainRoutesList = new ArrayList<>();
		trainRoutesList.add(new TrainRoute(new City("A"), new City("B"), 5));
		trainRoutesList.add(new TrainRoute(new City("B"), new City("C"), 4));
		trainRoutesList.add(new TrainRoute(new City("C"), new City("D"), 8));
		trainRoutesList.add(new TrainRoute(new City("D"), new City("C"), 8));
		trainRoutesList.add(new TrainRoute(new City("D"), new City("E"), 6));
		trainRoutesList.add(new TrainRoute(new City("A"), new City("D"), 5));
		trainRoutesList.add(new TrainRoute(new City("C"), new City("E"), 2));
		trainRoutesList.add(new TrainRoute(new City("E"), new City("B"), 3));
		trainRoutesList.add(new TrainRoute(new City("A"), new City("E"), 7));
		
		return trainRoutesList;
	}
	
}
