package com.thoughworks.exercise.service.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.thoughworks.exercise.train.exception.InputException;
import com.thoughworks.exercise.train.model.City;
import com.thoughworks.exercise.train.model.TrainRoute;
import com.thoughworks.exercise.train.service.RouteService;

public class RouteServiceDistanceTest {

	@Test
	public void distanceRouteTestA() throws InputException {
		String cities = "A-B-C";
		List<TrainRoute> trainRouteList = returnRoutes();
		int distance = RouteService.calculateDistance(cities, trainRouteList);
		assertEquals(9, distance);
	}
	
	@Test
	public void distanceRouteTestB() throws InputException {
		String cities = "A-D";
		List<TrainRoute> trainRouteList = returnRoutes();
		int distance = RouteService.calculateDistance(cities, trainRouteList);
		assertEquals(5, distance);
	}
	
	@Test
	public void distanceRouteTestC() throws InputException {
		String cities = "A-D-C";
		List<TrainRoute> trainRouteList = returnRoutes();
		int distance = RouteService.calculateDistance(cities, trainRouteList);
		assertEquals(13, distance);
	}
	
	@Test
	public void distanceRouteTestD() throws InputException {
		String cities = "A-E-B-C-D";
		List<TrainRoute> trainRouteList = returnRoutes();
		int distance = RouteService.calculateDistance(cities, trainRouteList);
		assertEquals(22, distance);
	}
	
	@Test
	public void distanceRouteTestE() throws InputException {
		String cities = "A-E-D";
		List<TrainRoute> trainRouteList = returnRoutes();
		int distance = RouteService.calculateDistance(cities, trainRouteList);
		assertEquals(-1, distance);
	}
	
	public List<TrainRoute> returnRoutes(){
		List<TrainRoute> trainRoutesList = new ArrayList<>();
		trainRoutesList.add(new TrainRoute(new City("A"), new City("B"), 5));
		trainRoutesList.add(new TrainRoute(new City("B"), new City("C"), 4));
		trainRoutesList.add(new TrainRoute(new City("C"), new City("D"), 8));
		trainRoutesList.add(new TrainRoute(new City("D"), new City("C"), 8));
		trainRoutesList.add(new TrainRoute(new City("D"), new City("E"), 6));
		trainRoutesList.add(new TrainRoute(new City("A"), new City("D"), 5));
		trainRoutesList.add(new TrainRoute(new City("C"), new City("E"), 2));
		trainRoutesList.add(new TrainRoute(new City("E"), new City("B"), 3));
		trainRoutesList.add(new TrainRoute(new City("A"), new City("E"), 7));
		
		return trainRoutesList;
	}
	
}
