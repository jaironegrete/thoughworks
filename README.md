# thoughworks
# ##### **Train_Java**

Requirements
- Java 1.8+
- mvn 3.5+

Build
- 	- cd Train-java
- 	- mvn clean install
- 	- mvn exec:java
- 	- follow the menu instructions

About Train_Java

I made a solution oriented to objects with the entities TrainRoute and City, the group of this classes made a graph with a Origin city and a Destination City, and a distance.
I try to solve all the problems with a base algorithm, the algorithm return a List of List of TrainRoutes according a group of params, like the option choosen and the math operator.
example:

0: AB3, BC4, CE2
1: BD3
2: CE6, EA3

with this List of List i manage the differentes requirements, for example if i want to calculate the shortest rout, i iterate this base and recover the shortest route accordind a distance param.
If i want to recover the number of routes between cities according a param, i iterate the base and recover the size of the list.

this exercise was made with with TDD, and i tried to follow  SOLID principles, i refactor the code a few times according the problem that i tried to solve.
if you want to see the project in bitbucket clone this : 
git clone https://jaironegrete@bitbucket.org/jaironegrete/thoughworks.git
you could see the commits, and the refactor process.

The exercises was divided in packages according a specifict funtion, the classes in the package manage a single responsability.
The testClasses was designed to solve the specific problems given in the exercises, and the exception in the classes are covered too.
